### Table Of Contents

1. [Introduction](#basic-data-types): Basic Data Types and Operators
1. [Variables](#variables)
1. [If](#if)
1. [Debugger](#debugger)
1. [Intro to Functions](#intro-functions)
1. [Functions](#functions)
1. [Intro to Objects](#intro-objects)
1. [Intro to DOM](#DOM)
1. [Loops](#loops)

Direct link to lessons: [1](#lesson1) [2](#lesson2) [3](#lesson3) [4](#lesson4) [5](#lesson5) [6](#lesson6) [7](#lesson7) [8](#lesson8) [9](#lesson9) [10](#lesson10) [11](#lesson11) [12](#lesson12) [13](#lesson13) [14](#lesson14) [15](#lesson15) [16](#lesson16) [17](#lesson17)

NOTE: when we have too many entries that don't fit on one screen
we can use this <!-- .slide: style="font-size:80%" -->
